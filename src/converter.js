/**
 * Padding outputs two characters allways
 * @param {string} hex one or two characters 
 * @returns {string} hex with two characters
 */
const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex);
}

module.exports = {
    /**
     * converts RGB to Hex string
     * @param {number} red 0-255
     * @param {number} green 0-255
     * @param {number} blue 0-255    
     */
    rgbToHex:(red, green, blue) => {
        const redHex = red.toString(16); // 0-255 -> 0-ff
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);
        const hex = pad(redHex) + pad(greenHex) + pad(blueHex);
        return hex;
    },
    /**
     * Converts Hex to RGB
     * // 0-ff -> 0-255
     * @param {number} hex 0-ffffff
     * @returns {string} r,g,b
     */
    hexToRgb:(hex) => {        
        var bigint = parseInt(hex, 16);
        var r = (bigint >> 16) & 255;
        var g = (bigint >> 8) & 255;
        var b = bigint & 255;        
        return r + "," + g + "," + b;
    }

}