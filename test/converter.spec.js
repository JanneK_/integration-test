// TDD - unit testing
const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color code converter", () => {
    describe("RGB to Hex conversion", () => {
        it("Converts the basic colors", () => {
            const redHex = converter.rgbToHex(255,0,0);
            const greenHex = converter.rgbToHex(0,255,0);
            const bluehex = converter.rgbToHex(0,0,255);
            
            expect(redHex).to.equal("ff0000");
            expect(greenHex).to.equal("00ff00");
            expect(bluehex).to.equal("0000ff");
        });
    });
    describe("Hex to RGB conversion", () => {
        it("Converts the basic colors", () => {
            const hexToRgb = converter.hexToRgb("ffffff");
            
            expect(hexToRgb).to.equal("255,255,255");            
        });
    });
});